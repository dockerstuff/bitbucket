FROM felixkazuyade/java-base
MAINTAINER Felix Kazuya felix.kazuya@icloud.com

# Umgebungsvariablen
ENV BITBUCKET_HOME="/var/atlassian/application-data/bitbucket"
ENV BITBUCKET_INSTALL_DIR="/opt/atlassian/bitbucket"
ENV BITBUCKET_VERSION="5.2.2"
ENV DOWNLOAD_URL="https://downloads.atlassian.com/software/stash/downloads/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz"


#Date of Build
RUN echo "Built at" $(date) > /etc/built_at


# Portfreigaben
EXPOSE 9080
EXPOSE 8080
EXPOSE 7999
EXPOSE 7990


# Dateien reinkopieren
#COPY entrypoint /entrypoint
#RUN chmod +x /entrypoint


# Anwendungen
RUN mkdir -p ${BITBUCKET_INSTALL_DIR}
RUN curl -L --silent ${DOWNLOAD_URL} | tar -xz --strip=1 -C ${BITBUCKET_INSTALL_DIR}
RUN mkdir -p ${BITBUCKET_INSTALL_DIR}/conf/Catalina
RUN chmod -R 700 ${BITBUCKET_INSTALL_DIR}/conf/Catalina
#RUN chmod -R 700 ${BITBUCKET_INSTALL_DIR}/logs
#RUN chmod -R 700 ${BITBUCKET_INSTALL_DIR}/temp
#RUN chmod -R 700 ${BITBUCKET_INSTALL_DIR}/work
RUN ln -s /usr/lib/x86_64-linux-gnu/libtcnative-1.so ${BITBUCKET_INSTALL_DIR}/lib/native/libtcnative-1.so


WORKDIR ${BITBUCKET_INSTALL_DIR}

CMD ["./bin/start-bitbucket.sh", "-fg"]